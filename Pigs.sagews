︠2735fde7-ce16-4531-b306-a2611641b5c6s︠
#Frequencies from the histograms made on Monday 5/6/2019
## Dot, NoDot, Razor,Trottor, Sounter, Leaning Jowler, Other
F=matrix([[7,8,26,5,2,0,3], [37, 52, 55, 9, 4, 1,4], [30,22, 29, 3, 0, 0,0], [33, 44, 34, 4, 0,0,0], [29, 34, 40, 12, 1, 0,0], [11, 21, 29, 11, 2, 2,3], [15, 15, 30, 12, 3, 0,0], [17, 29, 35, 6, 4, 1,9], [23,21, 24, 11, 2,1,5], [20, 27, 18, 5, 1, 0,4], [16, 20, 30, 14, 7, 1,1], [28, 40, 36, 8, 2, 0,0], [10, 30, 27, 9, 7, 0,9]])
F
︡06d1c4a5-7529-441a-822c-88571b94b69e︡{"stdout":"[ 7  8 26  5  2  0  3]\n[37 52 55  9  4  1  4]\n[30 22 29  3  0  0  0]\n[33 44 34  4  0  0  0]\n[29 34 40 12  1  0  0]\n[11 21 29 11  2  2  3]\n[15 15 30 12  3  0  0]\n[17 29 35  6  4  1  9]\n[23 21 24 11  2  1  5]\n[20 27 18  5  1  0  4]\n[16 20 30 14  7  1  1]\n[28 40 36  8  2  0  0]\n[10 30 27  9  7  0  9]\n"}︡{"done":true}
︠71e461aa-e184-4033-a7e6-2724fed83f4fs︠
## Score function: Feel free to modify or reimplement this code however you want to.

def Score (x, roll):
    if roll ==  [1,1]:                   ## sider with dots showing
        return x+1
    elif  roll ==  [2,2]:              ## sider with nodots showing
        return x+1
    elif  roll ==  [3,3]:             ## double razor
        return x+ 20
    elif  roll ==  [4,4]:             ## double trotter
        return x+20
    elif  roll ==  [5,5]:            ## double snouter
        return x+40
    elif  roll ==  [6,6]:             ## double leaning jowler
        return x+60
    elif  roll ==  [1,2]:   #pig out
        return 0
    elif  roll ==  [1,3]:   # razor
        return x + 5 
    elif  roll ==  [1,4]:   # trotter
        return x + 5 
    elif  roll ==  [1,5]:   #snouter
        return x + 10 
    elif  roll ==  [1,6]:   # leaning jowler
        return x + 15 
    elif  roll ==  [2,3]:   # razor
        return x + 5 
    elif  roll ==  [2,4]:   # trotter
        return x + 5 
    elif  roll ==  [2,5]:   #snouter
        return x + 10 
    elif  roll ==  [2,6]:   # leaning jowler
        return x + 15 
    elif  roll ==  [3,4]:   # razor plus trotter
        return x + 5+5 
    elif  roll ==  [3,5]:   #razor plus snouter
        return x + 5+10 
    elif  roll ==  [3,6]:   #razor plus leaning jowler
        return x + 5+ 15 
    elif  roll ==  [4,5]:   #trotter plus snouter
        return x + 5+10 
    elif  roll ==  [4,6]:   #trotter plus leaning jowler
        return x + 5+ 15
    elif  roll == [5,6]:    #trotter plus leaning jowler
        return x + 10+ 15 
    elif  (roll[0] == 7)  or (roll[1] == 7 ):   #other rolled, reroll with no penalty
        return x
    elif roll[0]>roll[1]:
        print ("here")
        return Score(x,[roll[1],roll[0]])
︡97e0c702-ae49-4548-b202-dac835cebeae︡{"done":true}
︠14b8c4f3-9ee4-45da-b768-4b8c6d472964s︠
counts=[sum(F.column(j)) for j in range(7)]
counts
︡f5d8aa71-71b5-4ff6-a01e-aafc5ee24241︡{"stdout":"[276, 363, 413, 109, 35, 6, 38]\n"}︡{"done":true}
︠c8518ed6-2b15-4b36-943a-b1efb0129386s︠
P=[counts[j]/sum(counts) for j in range(7)]
[float(p) for p in P]
︡bda432fa-75ce-4362-8240-acc18021bc96︡{"stdout":"[0.22258064516129034, 0.29274193548387095, 0.33306451612903226, 0.08790322580645162, 0.028225806451612902, 0.004838709677419355, 0.03064516129032258]\n"}︡{"done":true}
︠f582339a-988f-4029-973d-19870c343aa9s︠
sum(P)
︡49f481d8-1a60-4e7e-b8fd-22200062bd3a︡{"stdout":"1\n"}︡{"done":true}
︠71051011-c92e-4dff-9445-43da055f38cfs︠
random()
︡c7e8d645-4cbd-4cd2-a24b-90fa647bd29c︡{"stdout":"0.4595552415313364\n"}︡{"done":true}
︠f9380890-6324-478e-b622-1e1531fb4815s︠
CDF = [sum(P[i] for i in range(j)) for j in range(7)]   
[float(c) for c in CDF]+[1]  #cumulative distribution function for 7 possibilities is given by list of 8 numbers
︡933edd15-98a6-4b90-844a-52afd4fc5a5c︡{"stdout":"[0.0, 0.22258064516129034, 0.5153225806451613, 0.8483870967741935, 0.9362903225806452, 0.964516129032258, 0.9693548387096774, 1]\n"}︡{"done":true}
︠d914d26d-3081-459b-94d1-7d9da460b4c0s︠
def pigroll():
    x = random()
    roll =-1
    for i in range(7):
        if x>CDF[i]:
            roll = roll + 1
    return roll
︡5ddf6fa5-37e2-4246-a639-f3758aa3ed5b︡{"done":true}
︠4ab93f7b-41fd-4e95-9059-f3ede767e968s︠
[pigroll() for i in range(100)]
︡cbc13108-9b7d-466f-bcfe-8fe79d60b5cc︡{"stdout":"[1, 1, 7, 3, 1, 3, 1, 7, 2, 2, 1, 6, 2, 3, 3, 1, 2, 3, 3, 2, 3, 2, 5, 4, 2, 3, 1, 1, 2, 1, 1, 2, 1, 1, 3, 3, 3, 3, 2, 2, 4, 3, 2, 3, 5, 3, 3, 7, 1, 2, 3, 2, 5, 1, 3, 3, 2, 4, 2, 3, 4, 3, 4, 1, 3, 2, 2, 1, 2, 1, 2, 1, 2, 2, 4, 3, 3, 3, 1, 3, 3, 2, 4, 2, 2, 3, 1, 1, 4, 3, 4, 3, 5, 3, 1, 4, 2, 2, 3, 2]\n"}︡{"done":true}
︠32e72ac7-ea52-44cb-ad88-1437ca71f062s︠
Test = [0 for i in range(7)]   ## Looks like we are getting some numbers larger and some smaller than they should be
for i in range(1000):
    temp = pigroll()
    Test[temp] = Test[temp]+1
Test
︡8b719047-2c40-461b-ac1a-be8b0f9de925︡{"stderr":"Error in lines 2-4\nTraceback (most recent call last):\n  File \"/cocalc/lib/python2.7/site-packages/smc_sagews/sage_server.py\", line 1188, in execute\n    flags=compile_flags) in namespace, locals\n  File \"\", line 3, in <module>\nIndexError: list index out of range\n"}︡{"done":true}
︠f88dd041-2990-40a1-b64f-24628aeefb82s︠
[float(p) for p in P]
︡e5c96b30-a5f3-4ef7-9307-f129d81c90c2︡{"stdout":"[0.22258064516129034, 0.29274193548387095, 0.33306451612903226, 0.08790322580645162, 0.028225806451612902, 0.004838709677419355, 0.03064516129032258]\n"}︡{"done":true}
︠e4c162d3-2b7f-48ad-bd2c-389b39b1a6f1s︠
Test2 = [0 for i in range(7)]   ## looks better!
for i in range(10000):
    temp = pigroll()
    Test2[temp] = Test2[temp]+1
Test2
︡082c9d17-5a05-46b7-aebc-b4c36b0c5d04︡{"stdout":"[2219, 2918, 3329, 905, 277, 43, 309]\n"}︡{"done":true}
︠49a56f64-2fcb-467d-973c-a70bd5cb5e45s︠
roll = [pigroll()+1, pigroll()+1]
print(roll)
Score(3, roll)
︡290a3423-8e4e-4ec6-b9b2-4e9da5775f76︡{"stdout":"[1, 5]\n"}︡{"stdout":"13\n"}︡{"done":true}
︠7b5c15b2-06df-4b29-86f5-b75a8681c7f2︠









