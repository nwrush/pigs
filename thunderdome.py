# Nikko Rush
# 5/9/2019

import collections
import itertools
import multiprocessing as mp

import simulator


NUM_GAMES = 2                   


def thunderdome(decision_functions):
    results = collections.defaultdict(int)
    cnt = 0
    for a_func, b_func in itertools.combinations(decision_functions, 2):
        cnt += 1
        
        res = run_comparison(a_func, b_func)
        if res == 0:
            results[a_func] = 0
            results[b_func] = 0
        elif res > 0:
            results[a_func] += 1
        else:
            results[b_func] += 1

    print(cnt)
    print(max(results, key= lambda x: results[x]).__name__)


def run_comparison(a_func, b_func, num_games=NUM_GAMES):
    # num_games: Number of games each set should play with each player in each starting pos, actual number will be 2 times this value
    score = 0
    results = [0,0]
    for i in range(num_games):
        res = simulator.play_game(a_func, b_func, silent=True)
        score += res
        if res > 0:
            results[0] += 1
        else:
            results[1] += 1
            
    
    for i in range(num_games):
        # the returned value from play_game is negative is the second position wins so we need to multiply by -1
        # to match up with the value from the first simulation
        res = -1*simulator.play_game(b_func, a_func, silent=True)
        score += res

        if res > 0:
            results[0] += 1
        else:
            results[1] += 1

    return score, results


def generate_turn_cut_offs():
    funcs = list()
    for i in range(1, 101):
        def turn_cut_off(opp_score, my_score, turn_score, rounds):
            if my_score+turn_score >= 100:
                return False

            if turn_score >= i:
                return False

            return True
        turn_cut_off.__name__ = "turn_cut_off_"+str(i)
        funcs.append(turn_cut_off)

    return funcs
    

if __name__ == "__main__":
    thunderdome(generate_turn_cut_offs())
