# Nikko Rush
# 5/9/2019

import random


COUNTS = [276,363,413,109,35,6] # Dot, No Dot, Razor, Trotter, Snout, Leaning
PROBAB = [i/sum(COUNTS) for i in COUNTS]

CDF = list()
s = 0
for i in PROBAB:
    s += i
    CDF.append(s)

    
def roll_pig():
    roll = random.random()
    

    for i, prob in enumerate(CDF):
        if roll <= prob:
            return i

    raise ValueError()


def test_roll_pig():
    results = [0]*6

    num_rolls = 100000
    
    for i in range(num_rolls):
        roll = roll_pig()
        results[roll] += 1

    print(PROBAB)
    print([res/num_rolls for res in results])


def gen_score_dict():
    scores = dict()
    # Pig out
    scores[(0,1)] = -1
    scores[(1,0)] = -1

    # Slider
    scores[(0,0)] = 1
    scores[(1,1)] = 1

    # Single Razorback
    scores[(0,2)] = 5
    scores[(1,2)] = 5
    scores[(2,0)] = 5
    scores[(2,1)] = 5

    # Double Razorback
    scores[(2,2)] = 20

    # Single Trotter
    scores[(0,3)] = 5
    scores[(1,3)] = 5
    scores[(3,0)] = 5
    scores[(3,1)] = 5

    # Double Trotter
    scores[(3,3)] = 20

    #Single Snouter
    scores[(0,4)] = 10
    scores[(1,4)] = 10
    scores[(4,0)] = 10
    scores[(4,1)] = 10

    # Double Snouter
    scores[(4,4)] = 40

    # Single Leaning
    scores[(0,5)] = 15
    scores[(1,5)] = 15
    scores[(5,0)] = 15
    scores[(5,1)] = 15

    # Double Leaning
    scores[(5,5)] = 60

    # Mixed Combos
    scores[(2,3)] = 10          # Razor+Trotter
    scores[(3,2)] = 10

    scores[(2,4)] = 15          # Razor+Snout
    scores[(4,2)] = 15

    scores[(2,5)] = 20          # Razor+Leaning
    scores[(5,2)] = 20

    scores[(3,4)] = 15          # Trot+Snout
    scores[(4,3)] = 15

    scores[(3,5)] = 20          # Trot+Leaning
    scores[(5,3)] = 20

    scores[(4,5)] = 25          # Snout + Leaning
    scores[(5,4)] = 25

    return scores


SCORE_DICT = gen_score_dict()


def roll(turn_score):
    # Rolls two pigs and returns (new turn score, True/False if the turn is over)

    a = roll_pig()
    b = roll_pig()

    roll_points = SCORE_DICT[(a,b)]

    if roll_points < 0:
        return 0, False
    else:
        return turn_score + roll_points, True


    
def take_turn(opp_score, curr_score, decs_func):
    round = 0
    turn_score = 0
    while decs_func(opp_score, curr_score, turn_score, round):
        turn_score, cont = roll(turn_score)

        if not cont:
            return 0

    return curr_score + turn_score


def play_game(a_decs_func, b_decs_func, silent=False):
    player_a_score = 0
    player_b_score = 0

    turn = 0
    while True:
        player_a_score = take_turn(player_b_score, player_a_score, a_decs_func)

        if player_a_score >= 100:
            if not silent:
                print("a: {0}\tb: {1}".format(player_a_score, player_b_score))
                print("Player a won on turn: " + str(turn))
            return 1

        player_b_score = take_turn(player_a_score, player_b_score, b_decs_func)

        if player_b_score >= 100:
            if not silent:
                print("a: {0}\tb: {1}".format(player_a_score, player_b_score))
                print("Player b won on turn: " + str(turn))
            return -1

        turn += 1


def naive_player(opp_score, my_score, turn_score, round):
    if my_score+turn_score >= 100:
        return False
    return True


def no_roll(opp_score, my_score, turn_score, round):
    return False


if __name__ == "__main__":
    play_game(naive_player, no_roll)
        
