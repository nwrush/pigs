# Nikko Rush
# Copied from class provided sagews

import collections
import random

import thunderdome


COUNTS = [276,363,413,109,35,6]
P = [count/sum(COUNTS) for count in COUNTS]
CDF = [sum(P[i] for i in range(j)) for j in range(len(P)+1)]


def Score(x, roll):
    # Moving this check to the top of the function saves useless comparisons
    if roll[0]>roll[1]:
        return Score(x, [roll[1],roll[0]])

    if roll ==  [1,1]:                   ## sider with dots showing
        return x+1
    elif  roll ==  [2,2]:              ## sider with nodots showing
        return x+1
    elif  roll ==  [3,3]:             ## double razor
        return x+ 20
    elif  roll ==  [4,4]:             ## double trotter
        return x+20
    elif  roll ==  [5,5]:            ## double snouter
        return x+40
    elif  roll ==  [6,6]:             ## double leaning jowler
        return x+60
    elif  roll ==  [1,2]:   #pig out
        return 0
    elif  roll ==  [1,3]:   # razor
        return x + 5
    elif  roll ==  [1,4]:   # trotter
        return x + 5
    elif  roll ==  [1,5]:   #snouter
        return x + 10
    elif  roll ==  [1,6]:   # leaning jowler
        return x + 15
    elif  roll ==  [2,3]:   # razor
        return x + 5
    elif  roll ==  [2,4]:   # trotter
        return x + 5
    elif  roll ==  [2,5]:   #snouter
        return x + 10
    elif  roll ==  [2,6]:   # leaning jowler
        return x + 15
    elif  roll ==  [3,4]:   # razor plus trotter
        return x + 5+5
    elif  roll ==  [3,5]:   #razor plus snouter
        return x + 5+10
    elif  roll ==  [3,6]:   #razor plus leaning jowler
        return x + 5+ 15
    elif  roll ==  [4,5]:   #trotter plus snouter
        return x + 5+10
    elif  roll ==  [4,6]:   #trotter plus leaning jowler
        return x + 5+ 15
    elif  roll == [5,6]:    #trotter plus leaning jowler
        return x + 10+ 15
    elif  (roll[0] == 7)  or (roll[1] == 7 ):   #other rolled, reroll with no penalty
        return x

    print(roll)
    raise ValueError("Invalid roll provided")


def pigroll():
    x = random.random()
    roll = -1
    for i in range(7):
        if x>CDF[i]:
            roll = roll+1
    return roll


def test_rolls(n=1000000):
    rolls = [0]*len(P)

    for i in range(n):
        rolls[pigroll()] += 1

    print(P)
    print([roll/n for roll in rolls])


def hw5_prob5(n=100000):
    points = collections.defaultdict(int)

    for i in range(n):
        roll = [pigroll()+1, pigroll()+1] # to account for some absolute wackyness in how the points are calculated
        score = Score(0, roll)
        points[score] += 1

    for k,v in points.items():
        print("{0}: {1}, {2}".format(k, v, v/n))


def hw5_prob6_expected_score_method(opp_score, my_score, turn_score, rounds):
    # Using the expected value function from hw, q5
    if my_score+turn_score >= 100:
        return False
    
    expected_val = 0.861*turn_score+6.5

    return expected_val > turn_score # roll until you expect to lose points on the next roll


def hw5_prob6_greedy_method(opp_score, my_score, turn_score, rounds):
    return my_score+turn_score < 100


def hw5_prob6():
    score = thunderdome.run_comparison(hw5_prob6_expected_score_method, hw5_prob6_greedy_method, num_games=1000000)
    print(score)
    

if __name__ == "__main__":
    # test_rolls()
    # hw5_prob6(10000000)
    hw5_prob6()
    
