# Nikko Rush
# 5/9/2019

import unittest

import simulator


class TestSimulator(unittest.TestCase):

    def assertFloatListAlmostEqual(self, first, second, msg=None, delta=None):
        self.assertEqual(len(first), len(second), "lists are different sizes")

        for a, e in zip(first, second):
            self.assertAlmostEqual(a,e, msg=msg, delta=delta)
           
    def test_roll(self):
        num_rolls = 1000000
        results = [0]*6
        for i in range(num_rolls):
            results[simulator.roll_pig()] += 1

        self.assertFloatListAlmostEqual([res/num_rolls for res in results], simulator.PROBAB, delta=10**-2)


    def test_turn(self):
        for i in range(1000):            
            simulator.roll(0)

            
if __name__ == "__main__":
    unittest.main()
